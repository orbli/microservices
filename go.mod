module gitlab.com/orbli/microservice

go 1.12

require (
	cloud.google.com/go v0.44.0 // indirect
	github.com/go-redis/redis v0.0.0-20190503082931-75795aa4236d
	github.com/golang/protobuf v1.3.2
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/micro/go-micro v1.8.2
	github.com/nats-io/nats-server/v2 v2.0.2 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pborman/uuid v1.2.0
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
	google.golang.org/grpc v1.22.1 // indirect
)
