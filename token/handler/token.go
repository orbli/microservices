package handler

import (
	"context"
	"crypto/rand"
	"errors"
	"log"
	"time"

	"gitlab.com/orbli/microservice/token/model"
	pb "gitlab.com/orbli/microservice/token/proto"
	"gitlab.com/orbli/microservice/util/storage"
)

type (
	TokenService struct{}
)

var (
	_ pb.TokenServiceHandler = TokenService{}
)

func (TokenService) Create(ctx context.Context, req *pb.Token, res *pb.Token) error {
	token, err := pbToInternal(req)
	if err != nil {
		return err
	}

	token.Token = make([]byte, 32)
	rand.Read(token.Token)

	if token.Secret == nil {
		token.Secret = make([]byte, 32)
		rand.Read(token.Secret)
	}

	if token.ExpireAt == nil {
		expireAt := time.Now().AddDate(0, 0, 90)
		token.ExpireAt = &expireAt
	}

	if err := storage.Set(&token); err != nil {
		return err
	}

	pbv, err := internalToPb(token)
	if err != nil {
		return err
	}
	return TokenService{}.Read(ctx, pbv, res)
}

func (TokenService) Read(ctx context.Context, req *pb.Token, res *pb.Token) error {
	value := storage.Value(&model.Token{})
	if err := storage.Get(string(req.GetToken()), &value); err != nil {
		return err
	}
	pbv, err := internalToPb(*(value.(*model.Token)))
	if err != nil {
		return err
	}
	*res = *pbv
	return nil
}

func (TokenService) Update(ctx context.Context, req *pb.Token, res *pb.Token) error {
	token, err := pbToInternal(req)
	if err != nil {
		return err
	}
	value := storage.Value(&token)
	if err := storage.Set(value); err != nil {
		return err
	}
	return TokenService{}.Read(ctx, req, res)
}

func (TokenService) Delete(ctx context.Context, req *pb.Token, res *pb.Token) error {
	if err := storage.Delete(string(req.GetToken())); err != nil {
		return err
	}
	return nil
}

func (TokenService) DeleteParentedTokens(ctx context.Context, req *pb.Token, res *pb.Token) error {
	key := ""
	size := 10
	var listing func(string, int) ([]storage.Value, string, error)
	if store, ok := storage.Storage.(model.StorageGormSqlImpl); ok {
		listing = func(key string, size int) ([]storage.Value, string, error) {
			return store.ListByParent(req.Parent, key, size)
		}
	} else {
		log.Printf("Non SQL - rollback to old mechanism")
		listing = func(key string, size int) ([]storage.Value, string, error) {
			return storage.ListByKey(key, size)
		}
	}
	for {
		vs, next, err := listing(key, size)
		if err != nil {
			return err
		}
		if len(vs) == 0 {
			break
		}
		for _, v := range vs {
			if t, ok := v.(*model.Token); ok {
				if t.Parent == req.Parent {
					if err = storage.Delete(string(t.Token)); err != nil {
						return err
					}
				}
			} else {
				return errors.New("Unexpected error!")
			}
		}
		if key == "" {
			break
		}
		key = next
	}
	return nil
}
