package model

import (
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/orbli/microservice/util/storage"
)

type (
	StorageGormSqlImpl struct {
		db *gorm.DB
	}
	GormToken struct {
		Token
	}
)

var (
	_ storage.StorageStub = StorageGormSqlImpl{}
)

func NewStorageGormSql(dsn string) (*StorageGormSqlImpl, error) {
	conn, err := gorm.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	conn.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(GormToken{})
	conn.LogMode(true)
	conn.SetLogger(log.New(os.Stdout, "\n", 0))
	return &StorageGormSqlImpl{conn}, nil
}

func (s StorageGormSqlImpl) Get(key string, value *storage.Value) error {
	rt := new(GormToken)
	where := &GormToken{Token: Token{Token: []byte(key)}}
	if err := s.db.Where(where).First(rt).Error; err != nil {
		return err
	}
	*value = &rt.Token
	return nil
}

func (s StorageGormSqlImpl) Set(value storage.Value) error {
	v := &GormToken{Token: *(value.(*Token))}
	rt := s.db.Model(&GormToken{}).Updates(v)
	if rt.RowsAffected != 0 {
		return rt.Error
	}
	return s.db.Model(&GormToken{}).Create(v).Error
}

func (s StorageGormSqlImpl) Delete(key string) error {
	where := &GormToken{Token: Token{Token: []byte(key)}}
	return s.db.Where(where).Delete(&GormToken{}).Error
}

func (s StorageGormSqlImpl) ListByKey(key string, size int) ([]storage.Value, string, error) {
	rt := []storage.Value{}
	rtGorm := []*GormToken{}
	if err := s.db.Limit(size+1).Where("token >= ?", key).Find(&rtGorm).Error; err != nil {
		return nil, "", err
	}
	for _, v := range rtGorm {
		rt = append(rt, &v.Token)
	}
	if len(rt) < size+1 {
		return rt, "", nil
	}
	return rt[:len(rt)-1], string(rt[len(rt)].(*Token).Token), nil
}

func (s StorageGormSqlImpl) ListByParent(parent string, key string, size int) ([]storage.Value, string, error) {
	rt := []storage.Value{}
	rtGorm := []*GormToken{}
	if err := s.db.Limit(size+1).Where("parent = ? AND token >= ?", parent, key).Find(&rtGorm).Error; err != nil {
		return nil, "", err
	}
	for _, v := range rtGorm {
		rt = append(rt, &v.Token)
	}
	if len(rt) < size+1 {
		return rt, "", nil
	}
	return rt[:len(rt)-1], string(rt[len(rt)].(*Token).Token), nil
}
