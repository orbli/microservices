package model

import (
	"encoding/json"
	"time"

	"gitlab.com/orbli/microservice/util/storage"
)

type (
	Token struct {
		Token    []byte `gorm:"primary_key"`
		Secret   []byte
		Parent   string
		Data     []byte
		ExpireAt *time.Time
	}
)

var (
	_ storage.Value = &Token{}
)

func (m *Token) MarshalBinary() ([]byte, error) {
	return json.Marshal(m)
}

func (m *Token) UnmarshalBinary(d []byte) error {
	return json.Unmarshal(d, &m)
}

func (m *Token) Key() string {
	return string(m.Token)
}
