package model

import (
	"log"
	"os"

	"strconv"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/orbli/microservice/util/storage"
)

type (
	StorageGormSqlImpl struct {
		db *gorm.DB
	}
	GormUser struct {
		User
	}
)

var (
	_ storage.StorageStub = StorageGormSqlImpl{}
)

func NewStorageGormSql(dsn string) (*StorageGormSqlImpl, error) {
	conn, err := gorm.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	conn.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(GormUser{})
	conn.LogMode(true)
	conn.SetLogger(log.New(os.Stdout, "\n", 0))
	return &StorageGormSqlImpl{conn}, nil
}

func (s StorageGormSqlImpl) Get(key string, value *storage.Value) error {
	keyuint64, err := strconv.ParseUint(key, 10, 64)
	if err != nil {
		return err
	}
	rt := new(GormUser)
	where := &GormUser{User: User{Id: keyuint64}}
	if err := s.db.Where(where).First(rt).Error; err != nil {
		return err
	}
	*value = &rt.User
	return nil
}

func (s StorageGormSqlImpl) Set(value storage.Value) error {
	v := &GormUser{User: *(value.(*User))}
	rt := s.db.Model(&GormUser{}).Updates(v)
	if rt.RowsAffected != 0 {
		return rt.Error
	}
	return s.db.Model(&GormUser{}).Create(v).Error
}

func (s StorageGormSqlImpl) Delete(key string) error {
	keyuint64, err := strconv.ParseUint(key, 10, 64)
	if err != nil {
		return err
	}
	where := &GormUser{User: User{Id: keyuint64}}
	return s.db.Where(where).Delete(&GormUser{}).Error
}

func (s StorageGormSqlImpl) ListByKey(key string, size int) ([]storage.Value, string, error) {
	panic("Not yet implement je")
}
