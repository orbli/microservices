// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user/proto/user.proto

package orbli_micro_user

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	any "github.com/golang/protobuf/ptypes/any"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type User struct {
	Id                   uint64   `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Metadata             *any.Any `protobuf:"bytes,3,opt,name=metadata,proto3" json:"metadata,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_3ac6eb5dc4a0e42f, []int{0}
}

func (m *User) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_User.Unmarshal(m, b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_User.Marshal(b, m, deterministic)
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return xxx_messageInfo_User.Size(m)
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetId() uint64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *User) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *User) GetMetadata() *any.Any {
	if m != nil {
		return m.Metadata
	}
	return nil
}

func init() {
	proto.RegisterType((*User)(nil), "orbli.micro.user.User")
}

func init() { proto.RegisterFile("user/proto/user.proto", fileDescriptor_3ac6eb5dc4a0e42f) }

var fileDescriptor_3ac6eb5dc4a0e42f = []byte{
	// 221 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x90, 0x41, 0x4b, 0xc4, 0x30,
	0x10, 0x85, 0x4d, 0x0d, 0x41, 0x67, 0x41, 0x64, 0x50, 0xa9, 0x7b, 0x2a, 0x7b, 0xea, 0x69, 0x2a,
	0xeb, 0x45, 0x7a, 0x13, 0xfd, 0x05, 0x91, 0xbd, 0x79, 0x49, 0x37, 0xe3, 0x12, 0x68, 0x9b, 0x25,
	0x9b, 0x0a, 0xfd, 0xed, 0x5e, 0x24, 0x2d, 0x7a, 0x10, 0xbc, 0xf4, 0xf6, 0x78, 0x7c, 0xef, 0x4b,
	0x18, 0xb8, 0x1d, 0x4e, 0x1c, 0xaa, 0x63, 0xf0, 0xd1, 0x57, 0x29, 0xd2, 0x14, 0xf1, 0xda, 0x87,
	0xa6, 0x75, 0xd4, 0xb9, 0x7d, 0xf0, 0x94, 0xfa, 0xf5, 0xfd, 0xc1, 0xfb, 0x43, 0xcb, 0x33, 0xda,
	0x0c, 0x1f, 0x95, 0xe9, 0xc7, 0x19, 0xde, 0xbc, 0x83, 0xdc, 0x9d, 0x38, 0xe0, 0x15, 0x64, 0xce,
	0xe6, 0xa2, 0x10, 0xa5, 0xd4, 0x99, 0xb3, 0x88, 0x20, 0x7b, 0xd3, 0x71, 0x9e, 0x15, 0xa2, 0xbc,
	0xd4, 0x53, 0xc6, 0x07, 0xb8, 0xe8, 0x38, 0x1a, 0x6b, 0xa2, 0xc9, 0xcf, 0x0b, 0x51, 0xae, 0xb6,
	0x37, 0x34, 0x9b, 0xe9, 0xc7, 0x4c, 0xcf, 0xfd, 0xa8, 0x7f, 0xa9, 0xed, 0x97, 0x80, 0x55, 0xd2,
	0xbf, 0x71, 0xf8, 0x74, 0x7b, 0xc6, 0x1a, 0xd4, 0x4b, 0x60, 0x13, 0x19, 0xef, 0xe8, 0xef, 0x2f,
	0x29, 0x81, 0xeb, 0x7f, 0xfa, 0xcd, 0x19, 0x3e, 0x81, 0xd4, 0x6c, 0xec, 0x82, 0x65, 0x0d, 0x6a,
	0x77, 0xb4, 0xcb, 0x5e, 0xad, 0x41, 0xbd, 0x72, 0xcb, 0x4b, 0xb6, 0x8d, 0x9a, 0xae, 0xf2, 0xf8,
	0x1d, 0x00, 0x00, 0xff, 0xff, 0x9f, 0xbf, 0x3d, 0x08, 0xa8, 0x01, 0x00, 0x00,
}
